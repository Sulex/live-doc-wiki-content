## General
Also consider the general readme, contributing and licensing details on the [main page](https://gitlab.com/tum-fsd/document-wiki-content).

## Title
Document Baseline Panel

## Short Description
A document *Wiki Content* block that correctly lists all document baselines even when the document was moved or renamed in the past. For each baseline, the table includes the baseline name, a hyperlink to the document revision, the revision number (+ link to the *LiveDoc Compare View*), and the baseline time and date.

## Description
The code of this document *Wiki Content* block is based on the example provided in the *Advanced Querying* section of Polarion Help.

However, the original example code only shows the baselines for the current document location and name (ID). Consequently, previous baselines, where the document had a different name or resided in a different location, would not be displayed. Furthermore, baselines of other documents that used to have the documents current name would be displayed.

The code of this document *Wiki Content* block solves these two issues. It considers all previous document locations and names and also filters out baselines of other documents.

The document panel contains the baseline name, a link to the document revision, the revision number and the time and date of the baseline. For every baseline except the oldest, the revision number contains a hyperlink to the *LiveDoc Compare View* of the selected baseline and the previous.

The rendering section of the code can be modified to make the panel show the details that you need.

## Requirements
Polarion version >= 3.10.1

Tested with 21R1, 21R2

## Products
Polarion ALM/QA/REQ