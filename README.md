
# Document Wiki Content Code

This is a collection of usefull Polarion document *Wiki Content* code.

Each code block is located in its own folder.

Refer to the readme file in each folder for details and installation requirements.


## License

Download and use of the code is free. By downloading the code, you accept the license terms.

See [NOTICE.md](NOTICE.md) for licensing details.


## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) for contribution guidelines.


## Download & Installation

1. Download the source code of the *Wiki Content* block you are interested in.

1. Open the document on Polarion and add a new *Wiki Content* block.

1. Paste the source code in the editor and save.